# Conference IA

## Formation ENSTA ParisTech


You will find in this directory Jupyter notebooks to illustrate concepts and methods in Python (.ipynb files)

### Requirements to run notebooks:

  1) Python (> 3.3), Jupyter notebook and scikit-learn package. It is recommended to install them via <a href="https://www.anaconda.com/downloads">Anaconda Distribution</a>
    which will install these dependencies directly.
  2) *OR* use the mybinder service to run them remotely and interactively (open the link and wait a few seconds for the first connection so that the environment loads):
     -  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fconference-ia/d98a199f66d603b0b1e7c25fbe1341d29a40cd39?filepath=notebooks)
link  to run the introduction examples (Python, Fisher Iris dataset with K-Nearest Neighbors)
